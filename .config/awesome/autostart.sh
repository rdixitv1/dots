#!/usr/bin/env bash

xrandr -s 1920x1200 &
sxhkd &
killall volumeicon cbatticon
volumeicon 
nm-applet &
feh -zr --no-fehbg --bg-fill "$HOME/wallpapers/pics/rd"&
# dunst &
# flameshot &
picom &
emacs --daemon &
lxqt-policykit-agent &
# dunst &
