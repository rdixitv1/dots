#!/usr/bin/env python

# -*- coding: utf-8 -*-

import os
import socket
import subprocess

from libqtile import qtile
from qtile_extras import widget
from qtile_extras.widget.decorations import RectDecoration, PowerLineDecoration
from libqtile.config import (
    Click,
    Drag,
    Group,
    KeyChord,
    Key,
    Match,
    Screen,
    ScratchPad,
    DropDown,
)
from libqtile.lazy import lazy
from libqtile import layout, bar, hook
from libqtile.dgroups import simple_key_binder

run = "rofi -show drun -show-icons"
run2 = "rofi -show run -theme ~/.config/rofi/themes/top-tn.rasi"
if qtile.core.name == "x11":
    run = "rofi -show drun -show-icons -matching fuzzy"
    run2 = "rofi -show run -theme ~/.config/rofi/themes/top-tn.rasi"
elif qtile.core.name == "wayland":
    run = "wofi -S drun -I"
    run2 = "wofi -S run"

myBrowser = "firefox"
myBrowser2 = "brave-browser"
myBrowser3 = "/home/rd/.local/bin/zen-bin"
mod = "mod4"  # Sets mod key to SUPER/WINDOWS
myTerm = "alacritty"  # My terminal of choice
myTerm2 = "wezterm"  # My second terminal

colorBack = "#1a1b26"  # Bg
# colorFore = "#a9b1d6"  # Fg
colorFore = "#bbc2cf"  # Fg
# groupBoxFore = colorFore
groupBoxFore = "#282c34"

colorsTN = [
    ["#1a1b26", "#1a1b26"],
    ["#1c1f24", "#1c1f24"],
    ["#a9b1d6", "#a9b1d6"],
    ["#f7768e", "#f7768e"],
    ["#92ce6a", "#92ce6a"],
    ["#ff9364", "#ff9364"],
    ["#7dcfff", "#7dcfff"],
    ["#bb9af7", "#bb9af7"],
    ["#b4f9f8", "#b4f9f8"],
    ["#73daca", "#73daca"],
    ["#999999", "#999999"],
]

colorsOne = [
    ["#282c34", "#282c34"],
    ["#bbc2cf", "#bbc2cf"],
    ["#1c1f24", "#1c1f24"],
    ["#ff6c6b", "#ff6c6b"],
    ["#98be65", "#98be65"],
    ["#da8548", "#da8548"],
    ["#c678dd", "#c678dd"],
    ["#51afef", "#51afef"],
    ["#46d9ff", "#46d9ff"],
]

graysTN = [
    ["#2b2c37", "2b2c37"],
    ["#3c3d48", "3c3d48"],
    ["#4d4e59", "4d4e59"],
    ["#5e5f70", "5e5f70"],
    ["#6f7081", "6f7081"],
    ["#808192", "808192"],
]

graysOne = [
    ["#393d45", "393d45"],
    ["#4a4e56", "4a4e56"],
    ["#5b5f67", "5b5f67"],
    ["#6c7178", "6c7178"],
    ["#7d8289", "7d8289"],
    ["#abb2bf", "abb2bf"],
]

grays = graysTN
colors = colorsTN
hl = colors[7]

powerline1 = {
    "decorations": [
        PowerLineDecoration(
            path='arrow_right'
        )
    ]
}
powerline2 = {
    "decorations": [
        PowerLineDecoration(
            path='arrow_left'
        ),
    ]
}

decoration_group1 = {
    "decorations": [
        RectDecoration(
            colour=colors[4], radius=10, filled=True, padding_y=5, group=True
        ),
    ],
}
decoration_group2 = {
    "decorations": [
        RectDecoration(
            colour=hl, radius=10, filled=True, padding_y=5, group=True
        ),
    ],
}
decoration_group3 = {
    "decorations": [
        RectDecoration(
            colour=colors[3], radius=10, filled=True, padding_y=5, group=True
        ),
    ],
}
decoration_group4 = {
    "decorations": [
        RectDecoration(
            colour=colors[6], radius=10, filled=True, padding_y=5, group=True
        ),
    ],
}
decoration_group5 = {
    "decorations": [
        RectDecoration(
            colour=colors[5], radius=10, filled=True, padding_y=5, group=True
        ),
    ],
}


@lazy.layout.function
def add_treetab_section(layout):
    prompt = qtile.widgets_map["prompt"]
    prompt.start_input("Section name: ", layout.add_section)

@lazy.function
def fake_fullscreen(qtile):
    atom = set([qtile.conn.atoms["_NET_WM_STATE_FULLSCREEN"]])
    win = qtile.current_window.window
    state = set(win.get_property("NET_WM_STATE", "ATOM", unpack=int))
    if atom & state:
        state -= atom
    else:
        state |= atom
    win.set_property("_NET_WM_STATE", list(state))

@lazy.function
def minimize_all(qtile):
    for win in qtile.current_group.windows:
        if hasattr(win, "toggle_minimize"):
            win.toggle_minimize()


@lazy.function
def minimize(qtile):
    win = qtile.current_window
    if hasattr(win, "toggle_minimize"):
        win.toggle_minimize()


# START_KEYS
keys = [
    # The essentials
    Key([mod], "Return", lazy.spawn(myTerm), desc="Launches Terminal"),
    Key([mod, "shift"], "d", lazy.spawn(myTerm2),
        desc="Launches Second Terminal"),
    Key([mod, "shift"], "Return", lazy.spawn(run), desc="Run Launcher"),
    Key([mod, "mod1"], "Return", lazy.spawn(run2), desc="Run Launcher 2"),
    Key([mod, "mod1", "shift"], "Tab",
        lazy.spawn("rofi -show window"),
        desc="Rofi windows"),
    Key(["mod1"], "Tab", lazy.next_layout(), desc="Toggle through layouts"),
    Key(["mod1", "shift"], "Tab", lazy.prev_layout(),
        desc="Toggle through layouts"),
    Key([mod], "Tab", lazy.screen.next_group(), desc="Switch to next group"),
    Key(
        [mod, "shift"], "Tab", lazy.screen.prev_group(), desc="Switch to previous group"
    ),
    Key([mod, "shift"], "c", lazy.window.kill(), desc="Kill active window"),
    Key([mod, "shift"], "r", lazy.restart(), desc="Restart Qtile"),
    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload Qtile config"),
    Key([mod, "shift"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    # Switch focus to specific monitor (out of three)
    Key([mod, "mod1"], "1", lazy.to_screen(0),
        desc="Keyboard focus to monitor 1"),
    Key([mod, "mod1"], "2", lazy.to_screen(1),
        desc="Keyboard focus to monitor 2"),
    Key([mod, "mod1"], "3", lazy.to_screen(2),
        desc="Keyboard focus to monitor 3"),
    Key([mod], "period", lazy.next_screen(),
        desc="Move focus to next monitor"),
    Key([mod], "comma", lazy.prev_screen(), desc="Move focus to prev monitor"),
    # Treetab controls
    Key([mod, "control"], "h",
        lazy.layout.shuffle_left(),
        lazy.layout.move_left().when(layout=["treetab"]),
        desc="Move window to the left/move tab left in treetab"),

    Key([mod, "control"], "l",
        lazy.layout.shuffle_right(),
        lazy.layout.move_right().when(layout=["treetab"]),
        desc="Move window to the right/move tab right in treetab"),

    Key([mod, "control"], "j",
        lazy.layout.shuffle_down(),
        lazy.layout.section_down().when(layout=["treetab"]),
        desc="Move window down/move down a section in treetab"
        ),
    Key([mod, "control"], "k",
        lazy.layout.shuffle_up(),
        lazy.layout.section_up().when(layout=["treetab"]),
        desc="Move window downup/move up a section in treetab"
        ),
    # Window controls
    Key([mod], "h", lazy.layout.left(),
        desc="Move focus left in current stack pane"),
    Key([mod], "j", lazy.layout.down(),
        desc="Move focus down in current stack pane"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up in current stack pane"),
    Key([mod], "l", lazy.layout.right(),
        desc="Move focus right in current stack pane"),
    Key(
        [mod, "shift"],
        "j",
        lazy.layout.shuffle_down(),
        lazy.layout.section_down(),
        desc="Move windows down in current stack",
    ),
    Key(
        [mod, "shift"],
        "k",
        lazy.layout.shuffle_up(),
        lazy.layout.section_up(),
        desc="Move windows up in current stack",
    ),
    Key(
        [mod, "shift"],
        "h",
        lazy.layout.shuffle_left(),
        lazy.layout.section_left(),
        desc="Move windows left in current stack",
    ),
    Key(
        [mod, "shift"],
        "l",
        lazy.layout.shuffle_right(),
        lazy.layout.section_right(),
        desc="Move windows right in current stack",
    ),
    Key(
        [mod, "mod1"],
        "h",
        lazy.layout.shrink(),
        lazy.layout.decrease_nmaster(),
        lazy.layout.grow_left().when(layout=["bsp", "columns"]),
        desc="Shrink window (MonadTall), decrease number in master pane (Tile)",
    ),
    Key(
        [mod, "mod1"],
        "l",
        lazy.layout.grow(),
        lazy.layout.increase_nmaster(),
        lazy.layout.grow_right().when(layout=["bsp", "columns"]),
        desc="Expand window (MonadTall), increase number in master pane (Tile)",
    ),
    Key([mod, "mod1"], "k",
        lazy.layout.grow_up().when(layout=["bsp", "columns"]),
        lazy.layout.grow().when(layout=["monadtall", "monadwide"]),
        desc="Grow window upwards"
        ),
    Key([mod, "mod1"], "j",
        lazy.layout.grow_down().when(layout=["bsp", "columns"]),
        lazy.layout.shrink().when(layout=["monadtall", "monadwide"]),
        desc="Grow window downwards"
        ),
    Key(
        [mod, "mod1"], "n", lazy.layout.normalize(), desc="normalize window size ratios"
    ),
    Key(
        [mod],
        "m",
        lazy.layout.maximize(),
        desc="toggle window between minimum and maximum sizes",
    ),
    Key([mod], "t", lazy.window.toggle_floating(), desc="toggle floating"),
    Key([mod], "space", lazy.window.toggle_fullscreen(), desc="toggle fullscreen"),

    Key([mod], "minus",
        lazy.layout.grow_left().when(layout=["bsp", "columns"]),
        lazy.layout.grow().when(layout=["monadtall", "monadwide"]),
        desc="Grow window to the left"
        ),
    Key([mod], "equal",
        lazy.layout.grow_right().when(layout=["bsp", "columns"]),
        lazy.layout.shrink().when(layout=["monadtall", "monadwide"]),
        desc="Grow window to the left"
        ),
    Key([mod, "shift"], "minus",
        lazy.layout.grow_up().when(layout=["bsp", "columns"]),
        lazy.layout.grow().when(layout=["monadtall", "monadwide"]),
        desc="Grow window upwards"
        ),
    Key([mod, "shift"], "equal",
        lazy.layout.grow_down().when(layout=["bsp", "columns"]),
        lazy.layout.shrink().when(layout=["monadtall", "monadwide"]),
        desc="Grow window downwards"
        ),
    # Stack controls
    Key(
        [mod, "mod1"],
        "Tab",
        lazy.layout.rotate(),
        lazy.layout.flip(),
        desc="Switch which side main pane occupies (XmonadTall)",
    ),
    Key(
        [mod, "mod1"],
        "space",
        lazy.layout.next(),
        desc="Switch window focus to other pane(s) of stack",
    ),
    Key(
        [mod, "mod1", "shift"],
        "space",
        lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack",
    ),
    Key(
        [mod, "mod1", "shift", "control"], "l",
        lazy.layout.swap_column_right(),
        desc="Swap current column with the one to the right"
    ),
    Key(
        [mod, "mod1", "shift", "control"], "h",
        lazy.layout.swap_column_left(),
        desc="Swap current column with the one to the left"
    ),
    Key([mod, "shift"], "a", add_treetab_section,
        desc='Prompt to add new section in treetab'),

    # Media
    # Key([], "XF86AudioLowerVolume",
    #         lazy.spawn("pamixer -d 5"),
    # desc="Decrease volume"
    # ),
    # Key([], "XF86AudioRaiseVolume",
    #         lazy.spawn("pamixer -i 5"),
    # desc="Increase volume"
    # ),
    # Key([], "XF86AudioMute",
    #         lazy.spawn("pamixer -t"),
    # desc="Mute volume"
    # ),
    Key([], "Print", lazy.spawn("flameshot gui"), desc="Flameshot gui"),
    Key(["shift"], "Print", lazy.spawn("flameshot full"), desc="Flameshot full"),
    Key([mod], "Print", lazy.spawn("flameshot full -c"),
        desc="Flameshot full (copy)"),
    Key(["mod1"], "Print", lazy.spawn(
        "flameshot launcher"), desc="Flameshot launcher"),

    Key([], "XF86AudioPlay", lazy.spawn(
        "playerctl play-pause"), desc="Pause track"),
    Key([], "XF86AudioNext", lazy.spawn("playerctl next"), desc="Next track"),
    Key([], "XF86AudioPrev", lazy.spawn(
        "playerctl previous"), desc="Previous track"),
    Key(
        [],
        "XF86MonBrightnessUp",
        lazy.spawn("xbacklight -inc 5"),
        desc="Increase screen brightness",
    ),
    Key(
        [],
        "XF86MonBrightnessDown",
        lazy.spawn("xbacklight -dec 5"),
        desc="Decrease screen brightness",
    ),
    KeyChord([mod, "mod1"], "slash", [
        Key([mod, "mod1"], "slash", lazy.spawn(
            "feh -zr --bg-fill --no-fehbg /home/rd/wallpapers/pics/rd &")),
        Key([], "s", lazy.spawn(
            "feh -zr --bg-fill --no-fehbg /home/rd/wallpapers/pics/fav/TokyoNight &")),
        Key([], "a", lazy.spawn(
            "feh -zr --bg-fill --no-fehbg /home/rd/wallpapers/pics/fav/ &")),
    ]),



    # Scratchpad
    KeyChord([mod], "s", [
        Key(
            [], "t",
            lazy.group["Scratchpad"].dropdown_toggle("term"),
            desc="Terminal scratchpad",
        ),
        Key(
            [], "b",
            lazy.group["Scratchpad"].dropdown_toggle("browser"),
            desc="Browser scratchpad",
        ),
        Key(
            [],
            "m",
            lazy.group["Scratchpad"].dropdown_toggle("music"),
            desc="Music scratchpad",
        ),
        Key(
            [],
            "f",
            lazy.group["Scratchpad"].dropdown_toggle("fmger"),
            desc="GUI file manager scratchpad",
        ),
        Key(
            ["shift"],
            "f",
            lazy.group["Scratchpad"].dropdown_toggle("fm"),
            desc="Terminal file manager scratchpad",
        ),
        Key(
            [],
            "c",
            lazy.group["Scratchpad"].dropdown_toggle("calc"),
            desc="Calculator scratchpad",
        )
    ]),
    Key([mod, "shift"], "m", minimize(), desc="Minimize current window"),
    Key([mod, "mod1"], "m", minimize_all(),
        desc="Minimize all windows in current group"),
    Key([mod, "shift"], "f", fake_fullscreen(), desc="Fake fullscreen"),
    # Help
    Key(
        [mod],
        "slash",
        lazy.spawn("/home/rd/.config/qtile/qtile-keys"),
        desc="View Keybindings",
    ),
    Key(
        [mod, "shift"],
        "slash",
        lazy.spawn("/home/rd/.config/sxhkd/keys"),
        desc="View sxhkd keybindings"
    ),
    Key([mod], "d", lazy.spawncmd(), desc="Run Launcher"),
    Key([mod, "shift"], "b", lazy.hide_show_bar(), desc="Toggle bar visibility"),
    KeyChord([mod], "b", [
        Key([mod], "b",
            lazy.spawn(myBrowser)
            ),
        Key([], "b", lazy.spawn(myBrowser2)),
        Key(["shift"], "b", lazy.spawn(myBrowser3)),
        Key([], "f", lazy.spawn("pcmanfm")),
        Key(["shift"], "f", lazy.spawn("pcmanfmqt")),
        Key([], "z", lazy.spawn("zulip")),
        Key([mod], "n", lazy.spawn(myBrowser + " --private-window")),
        Key([], "n", lazy.spawn(myBrowser2 + " --incognito")),
		Key(["shift"], "n", lazy.spawn(myBrowser3 + " --private-window")),
        Key([], "w", lazy.spawn("alacritty -e nmtui")),
        Key([], "m", lazy.spawn("alacritty -e btop")),
        Key([mod], "f", lazy.spawn("alacritty -e lf")),
        Key([], "c", lazy.spawn("qalculate-gtk")),
        Key([], "p", lazy.spawn("pavucontrol")),
        KeyChord([], "x", [
            Key([], "1", lazy.spawn("xrandr -s 1920x1200")),
            Key([], "2", lazy.spawn("xrandr -s 2560x1600")),
            Key([], "3", lazy.spawn("xrandr -s 1400x900"))
        ]),
    ]),
    KeyChord([mod], "f", [
        KeyChord([], "f", [
            Key([], "f", lazy.spawn("flameshot full")),
            Key([], "s", lazy.spawn("flameshot gui"))
        ]),
        Key([], "c", lazy.spawn("flameshot full -c")),
        Key([], "g", lazy.spawn("flameshot launcher")),
    ]),
    Key([mod], "x", lazy.spawn(f"i3lock -c \"{colors[0][0]}\"")),
    KeyChord([mod], "e", [
        Key([mod], "e", lazy.spawn("emacsclient -c -a 'emacs'")),
        Key([], "e", lazy.spawn("emacsclient -c -a 'emacs'")),
        Key([], "b", lazy.spawn("emacsclient -c -a 'emacs', -e '(ibuffer)'")),
        Key([], "d", lazy.spawn("emacsclient -c -a 'emacs', -e '(dired nil)'")),
        Key([], "i", lazy.spawn("emacsclient -c -a 'emacs', -e '(erc)'")),
        Key([], "m", lazy.spawn("emacsclient -c -a 'emacs', -e '(mu4e)'")),
        Key([], "s", lazy.spawn("emacsclient -c -a 'emacs', -e '(eshell)'")),
        Key([], "v", lazy.spawn("emacsclient -c -a 'emacs', -e '(+verm/here nil)'")),
        Key(["shift"], "d", lazy.spawn("emacs --daemon")),
        Key([], "r",
            lazy.spawn("killall emacs"),
            lazy.spawn("/usr/bin/emacs --daemon"),
            desc="Restart the emacs daemon")
    ]),
    KeyChord([mod], "p", [
        Key([], "b", lazy.spawn("/home/rd/run-scripts/dm-bookmarks")),
        Key([], "e", lazy.spawn("/home/rd/run-scripts/dm-confedit")),
        Key([], "c", lazy.spawn("/home/rd/run-scripts/dm-currencies")),
        Key([], "h", lazy.spawn("/home/rd/run-scripts/dm-help")),
        Key(["shift"], "h", lazy.spawn("/home/rd/run-scripts/dm-history")),
        Key([], "space", lazy.spawn("/home/rd/run-scripts/dm-hub")),
        Key([], "k", lazy.spawn("/home/rd/run-scripts/dm-kill")),
        Key([], "l", lazy.spawn("/home/rd/run-scripts/dm-lg")),
        Key([], "o", lazy.spawn("/home/rd/run-scripts/dm-maim")),
        Key([], "d", lazy.spawn("/home/rd/run-scripts/dm-man")),
        Key([], "n", lazy.spawn("/home/rd/run-scripts/dm-note")),
        Key([], "r", lazy.spawn("/home/rd/run-scripts/dm-rec")),
        Key([], "t", lazy.spawn("dmenu-translate")),
        Key(["shift"], "t", lazy.spawn("/home/rd/run-scripts/dm-translate")),
        Key([], "g", lazy.spawn("/home/rd/run-scripts/dm-wall")),
        Key([], "w", lazy.spawn("/home/rd/run-scripts/dm-weather")),
        Key([], "s", lazy.spawn("/home/rd/run-scripts/dm-websearch")),
        Key(["shift"], "w", lazy.spawn("/home/rd/run-scripts/dm-wifi")),
        Key([], "p", lazy.spawn("/home/rd/run-scripts/dm-pass")),
        Key([], "q", lazy.spawn("/home/rd/run-scripts/dm-power")),
        Key([], "return", lazy.spawn("/home/rd/run-scripts/dm-menu")),
        Key([mod], "q", lazy.spawn("/home/rd/run-scripts/dm-qtile-theme")),
        Key([], "a", lazy.spawn("rofi -show calc -modi calc -now-show-match -no-sort")),
        Key([], "f", lazy.spawn("rofi -show filebrowser")),
    ]),
    KeyChord([mod, "shift"], "space", [
        Key([], "1", lazy.spawn("setxkbmap intl")),
        Key([], "2", lazy.spawn("setxkbmap mtg")),
        Key([], "3", lazy.spawn("setxkbmap wmn")),
        Key([], "4", lazy.spawn("setxkbmap dk")),
        Key([], "5", lazy.spawn("setxkbmap de")),
        Key([], "6", lazy.spawn("setxkbmap in -variant kan-kagapa")),
        Key([], "7", lazy.spawn("setxkbmap in -variant san-kagapa")),
        Key([], "8", lazy.spawn("setxkbmap ru -variant phonetic")),
        Key([], "9", lazy.spawn("setxkbmap il -variant phonetic")),
    ]),
    KeyChord([mod, "shift"], "u", [
        Key([mod, "shift"], "u", lazy.spawn("xbacklight =0")),
        Key([], "j", lazy.spawn("xbacklight -dec 1")),
        Key([], "k", lazy.spawn("xbacklight -inc 1")),
        Key([], "a", lazy.spawn("xbacklight =100")),
        Key(["shift"], "a", lazy.spawn("xbacklight =1")),
        Key([], "space", lazy.spawn("mpc toggle")),
        Key([], "n", lazy.spawn("mpc next")),
        Key([], "b", lazy.spawn("mpc prev")),
    ]),
    KeyChord([mod, "shift"], "z", [
        Key([], "j", lazy.spawn("xbacklight -dec 1")),
        Key([], "k", lazy.spawn("xbacklight -inc 1")),
    ],
        mode=True,
        name="media"
    ),
    KeyChord([mod], "z", [
        Key([], "h", lazy.layout.grow()),
        Key([], "j", lazy.layout.grow()),
        Key([], "k", lazy.layout.shrink()),
        Key([], "l", lazy.layout.shrink()),
        Key([], "u", lazy.layout.normalize()),
        Key([], "m", lazy.layout.maximize())],
        mode=True,
        name="windows"
    )
]
# END_KEYS

groups = [
    Group("1", layout="columns"),
    Group("2", layout="columns"),
    Group("3", layout="columns"),
    Group("4", layout="columns"),
    Group("5", layout="columns"),
    Group("6", layout="columns"),
    Group("7", layout="columns"),
    Group("8", layout="columns"),
    Group("9", layout="columns"),
    Group("10", layout="columns"),
]


groups.append(
    ScratchPad(
        "Scratchpad",
        [
            DropDown(
                "term", myTerm, width=0.75, height=0.9, x=0.125, y=0.05, opacity=1
            ),
            DropDown(
                "browser",
                myBrowser,
                width=0.75,
                height=0.9,
                x=0.125,
                y=0.05,
                opacity=1,
            ),
            DropDown(
                "music",
                myTerm + " -e ncmpcpp",
                width=0.75,
                height=0.9,
                x=0.125,
                y=0.05,
                opacity=1,
            ),
            DropDown(
                "fmger",
                "pcmanfm",
                width=0.75,
                height=0.85,
                x=0.125,
                y=0.05,
                opacity=1,
            ),
            DropDown(
                "fm",
                myTerm + " -e lf",
                width=0.75,
                height=0.85,
                x=0.125,
                y=0.075,
                opacity=1,
            ),
            DropDown(
                "calc", "qalculate-gtk", width=0.75, height=0.85, x=0.125, y=0.075, opacity=1
            ),
        ],
    )
)


# Allow MODKEY+[0 through 9] to bind to groups, see https://docs.qtile.org/en/stable/manual/config/groups.html
# MOD4 + index Number : Switch to Group[index]
# MOD4 + shift + index Number : Send active window to another Group

dgroups_key_binder = simple_key_binder("mod4")


layout_theme = {
    "border_width": 2,
    # "fullscreen_border_width": 0,
    "margin": 6,
    "border_focus": hl[0],
    "border_normal": grays[1][0],
}

layouts = [
    layout.MonadWide(**layout_theme, single_margin=0, single_border_width=0),
    layout.MonadTall(**layout_theme, single_margin=0, single_border_width=0),
    # layout.MonadTall(
    #     border_width=2,
    #     margin=0,
    #     border_focus=hl[0],
    #     border_normal=grays[1][0],
    #     single_margin=0,
    #     single_border_width=0),
    layout.Columns(
        margin_on_single=None,
        insert_position=1,
        single_border_width=0,
        border_focus=hl[0],
        border_focus_stack=colors[6],
        border_normal=grays[1][0],
        border_normal_stack=grays[1][0],
    ),
    layout.Max(
        border_width=0,
        margin=0,
    ),
    layout.Stack(num_stacks=2, border_focus=hl[0], border_normal=grays[2][0]),

    layout.MonadThreeCol(
        new_client_position="after_current",
        main_centered=False,
        ratio=0.45,
        single_margin=0,
        single_border_width=0,
        **layout_theme
    ),
    # layout.TreeTab(
    #     font="JetBrainsMono NF",
    #     fontsize=19,
    #     sections=["I", "II", "III", "IV", "V"],
    #     section_fontsize=19,
    #     border_width=2,
    #     bg_color=colorBack,
    #     active_bg="bb9af7",
    #     active_fg="000000",
    #     inactive_bg=grays[1],
    #     inactive_fg=colorFore,
    #     padding_left=0,
    #     padding_x=0,
    #     padding_y=5,
    #     section_top=10,
    #     section_bottom=20,
    #     level_shift=8,
    #     vspace=3,
    #     panel_width=200,
    # ),
    layout.Floating(
        border_width=2,
        border_focus="BB9AF7",
        border_normal=grays[1][0])
]

prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())

##### DEFAULT WIDGET SETTINGS #####
widget_defaults = dict(
    font="Ubuntu Nerd Font Bold", fontsize=16, padding=2, background=colors[2]
)
extension_defaults = widget_defaults.copy()


def init_widgets_list():
    widgets_list = [
        widget.Sep(linewidth=0, padding=6,
                   foreground=colors[2], background=colors[0]),
        # widget.GroupBox(
        #    font="Ubuntu Bold",
        #    fontsize=16,
        #    margin_y=3,
        #    margin_x=0,
        #    padding_y=5,
        #    padding_x=3,
        #    borderwidth=3,
        #    active=hl,
        #    inactive=colors[10],
        #    rounded=False,
        #    highlight_color=colors[0],
        #    highlight_method="line",
        #    this_current_screen_border=hl,
        #    this_screen_border=colors[4],
        #    other_current_screen_border=colors[8],
        #    other_screen_border=colors[4],
        #    foreground=colors[2],
        #    background=colors[0],
        # ),
        widget.TextBox(
            text='',
            font="JetBrainsMono NF",
            background=colors[0],
            foreground=colorFore,
            mouse_callbacks={
                "Button1": lambda: qtile.spawn(
                    "/home/rd/run-scripts/dm-menu"
                ),
                "Button3": lambda: qtile.spawn(
                    "rofi -show drun -show-icons"
                )
            },

            padding=0,
            fontsize=30,

        ),

        widget.Sep(linewidth=0, padding=12,
                   foreground=colors[0], background=colors[0], **powerline2),
        widget.GroupBox(
            font="JetBrainsMono NF",

            # background=grays[2],
            background=hl,

            fontsize=20,

            hide_unused=True,

            disable_drag=True,
            toggle=False,
            highlight_method="block",
            borderwidth=4,

            # this_current_screen_border=hl,
            # block_highlight_text_color=colors[0],  # block text color
            this_current_screen_border=colors[0],
            block_highlight_text_color=hl,  # block text color

            # active=hl,  # text color
            active=groupBoxFore,  # text color
            urgent_alert_method="block",
            urgent_border=colors[1],
            urgent_text=colors[1],
            inactive=colorFore,
            spacing=4,
            margin_x=6,
            margin_y=3,  # push labels down
            padding_x=2,
            padding_y=2,

            **powerline2
        ),

        widget.Sep(linewidth=0, padding=4,
                   foreground=colors[0], background=grays[1]),
        widget.CurrentLayoutIcon(
            custom_icon_paths=[os.path.expanduser("~/.config/qtile/icons")],
            foreground=colors[2],
            background=grays[1],
            padding=0,
            scale=0.7,
            **powerline2
        ),
        widget.Sep(linewidth=0, padding=3,
                   foreground=colors[0],
                   background=grays[1], **powerline2),
        widget.Systray(background=colors[0],
                       fontsize=30, padding=7,
                       icon_size=28),
        widget.Sep(linewidth=0, padding=4,
                   foreground=colors[0], background=colors[0], **powerline2),
        widget.Prompt(
            background=colors[0],
            cursor_blink=0,
            prompt="↪ ",
            bell_style="visual",
            visual_bell_color=colors[3],
            font="JetBrainsMono NF",
            fontsize=23,

        ),
        widget.Chord(
            foreground=colorFore,
            background=colors[0],
            font="JetBrainsMono NF",
            fontsize=20,
            # padding=6,
            chords_colors={
                'windows': (colors[6], colors[0]),
                'media': (colors[5], colors[0])},
            # **powerline2
        ),
        widget.Spacer(
            background=colors[0],
            **powerline1
        ),

        widget.Clock(
            foreground="#a9b1d6",
            font="JetBrainsMono NF Bold",
            background=grays[1],
            padding=10,
            format="%a, %b %d, %Y %H:%M:%S",
            fontsize=20,
            mouse_callbacks={
                "Button1": lambda: qtile.spawn(
                    "emacsclient -c -a 'emacs' --eval '(doom/window-maximize-buffer(cfw:open-org-calendar))'"
                )
            },
            **powerline2,
        ),
        widget.Spacer(
            background=colors[0],
        ),

        widget.Sep(
            linewidth=0,
            padding=6,
            foreground=colors[0],
            background=colors[0],
            **powerline1),

        widget.Net(
            format="\uf1eb  {down:.0f} {down_suffix} \uf175\uf176 {up:.0f} {up_suffix}",
            font="JetBrainsMono NF Bold",
            mouse_callbacks={
                "Button1": lambda: qtile.spawn("alacritty -e nmtui"),
                "Button3": lambda: qtile.spawn("wezterm -e nmtui"),
            },
            background=grays[1],
            foreground=colorFore,
            padding=8,
        ),
        widget.Spacer(length=3, background=grays[1], **powerline1),
        widget.CPU(
            format="\ue266 {load_percent:>2.0f}%",
            font="JetBrainsMono NF Bold",
            update_interval=5,
            padding=8,
            background=grays[2],
            foreground=colorFore,
            # decorations=[
            #     RectDecoration(
            #         colour=colors[8],
            #         radius=10,
            #         filled=True,
            #         padding_y=5
            #         # group=True
            #     )
            # ],
        ),
        widget.Spacer(length=3, background=grays[2], **powerline1),

        # widget.Spacer(length=1, background=grays[3], **powerline1),
        widget.TextBox(
            text="",
            font="Ubuntu Nerd Font",
            background=hl,
            foreground=colors[0],
            padding=4,
            fontsize=30,
            # **decoration_group3
        ),
        widget.KeyboardLayout(
            font="JetBrainsMono NF Bold",
            mouse_callbacks={
                "Button1": lambda: qtile.spawn("setxkbmap intl"),
                "Button2": lambda: qtile.spawn(
                    "setxkbmap wmn -variant workman-intl"
                ),
                "Button3": lambda: qtile.spawn("setxkbmap mtg"),
            },
            configured_keyboards=[
                "intl",
                "wmn",
                "hmk",
                "dk",
                "de",
                "in kan-kagapa",
                "in san-kagapa",
                "us",
                "ru phonetic",
                "il phonetic"
            ],
            display_map={
                "intl": "int",
                "wmn": "wkmn",
                "mtg": "mtg",
                "dk": "dk",
                "de": "de",
                "in kan-kagapa": "kn",
                "in san-kagapa": "san",
                "us": "us",
                "ru phonetic": "ru",
                "il phonetic": "il",
            },
            foreground=colors[0],
            background=hl,
            padding=4,
            # **decoration_group3
            **powerline1
        ),
        # widget.UPowerWidget(
        #     background=colors[0],
        #     foreground=colors[2],
        #     battery_height=16,
        #     battery_width=40,
        #     border_colour=colors[2],
        #     fill_normal=colorFore,
        #     border_charge_colour=colors[4],
        #     margin=5,
        # ),
			     widget.Battery(
			         font="JetBrainsMono NF Bold",
			         background=colors[0],
		 			foreground=colors[2],
			         discharge_char='-',
			         charge_char='+',
			         format='{percent:1.0%}{char} {hour:d}:{min:02d}',
			         hide_threshold=98
			     )
    ]
    return widgets_list


def init_widgets_screen1():
    widgets_screen1 = init_widgets_list()
    #    del widgets_screen1[7:8]
    # Slicing removes unwanted widgets (systray) on Monitors 1,3
    return widgets_screen1


def init_widgets_screen2():
    widgets_screen2 = init_widgets_list()
    # Monitor 2 will display all widgets in widgets_list
    return widgets_screen2


def init_widgets_wayland():
    widgets_wl = init_widgets_list()
    # del widgets_wl[24:25]
    return widgets_wl


def init_screens():
    return [Screen(
        top=bar.Bar(widgets=init_widgets_screen1(),
                    opacity=1.0, size=32))]


if __name__ in ["config", "__main__"]:
    screens = init_screens()
    widgets_list = init_widgets_list()

    widgets_screen1 = init_widgets_screen1()
    widgets_screen2 = init_widgets_screen2()


def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i - 1].name)


def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i + 1].name)


def window_to_previous_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i != 0:
        group = qtile.screens[i - 1].group.name
        qtile.current_window.togroup(group)


def window_to_next_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i + 1 != len(qtile.screens):
        group = qtile.screens[i + 1].group.name
        qtile.current_window.togroup(group)


def switch_screens(qtile):
    i = qtile.screens.index(qtile.current_screen)
    group = qtile.screens[i - 1].group
    qtile.current_screen.set_group(group)


mouse = [
    Drag(
        [mod],
        "Button1",
        lazy.window.set_position_floating(),
        start=lazy.window.get_position(),
    ),
    Drag(
        [mod], "Button3", lazy.window.set_size_floating(),
        start=lazy.window.get_size()
    ),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False

floating_layout = layout.Floating(
    border_focus=colors[6],
    border_normal=grays[1][0],
    border_width=2,
    float_rules=[
        # Run the utility of `xprop` to see the wm
        # class and name of an X client.
        # default_float_rules include: utility,
        # notification, toolbar, splash, dialog,
        # file_progress, confirm, download and error.
        *layout.Floating.default_float_rules,
        Match(title="Confirmation"),  # tastyworks exit box
        Match(title="Qalculate!"),  # qalculate-gtk
        Match(wm_class="pinentry-gtk-2"),  # GPG key password entry
        Match(wm_class="Yad"),
        Match(wm_class="pavucontrol"),
        Match(wm_class="zoom"),
        Match(wm_class="arandr")
    ],
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True
single_border_width = 0
auto_minimize = True


@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser("~")
    if qtile.core.name == "x11":
        subprocess.call([home + "/.config/qtile/autostart.sh"])
    elif qtile.core.name == "wayland":
        subprocess.call([home + "/.config/qtile/autostart-wayland.sh"])


wmname = "LG3D"
# vim: tabstop=4 shiftwidth=4 noexpandtab
