# Defined via `source`
function doomsync --wraps='~/.emacs.d/bin/doom sync; emacs' --description 'alias doomsync ~/.emacs.d/bin/doom sync; emacs'
  ~/.emacs.d/bin/doom sync; emacs $argv; 
end
