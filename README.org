#+TITLE: Dotfiles
#+AUTHOR: Raghav Dixit

[[https://gitlab.com/rdixitv1/dots/-/raw/master/screenshots/awesometn1.png]]

[[https://gitlab.com/rdixitv1/dots/-/raw/master/screenshots/awesometn2.png]]

Welcome to my dotfiles repository!
* What I actually use
I obviously don't use EVERYTHING in this repo.
| *Shell*          | Fish                   |
| *Terminal*       | Wezterm                |
| *Text Editor*    | Emacs (sometimes nvim) |
| *Run Launcher*   | Rofi                   |
| *Window Manager* | Awesome                |
