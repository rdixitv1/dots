local wez = require('wezterm')
return {
  font = wez.font('JetBrainsMono NF'),
  -- font = wez.font('Iosevka'), harfbuzz_features = {"cv45 3", "cv44 2", "cv89 6", "cv91 1", "cv31 11", "cv74 1"},
  -- font = wez.font('Fira Code NF'), harfbuzz_features = {"ss02", "ss04", "ss01", "cv14", "ss05"},
  colors = {
    foreground = "#a9b1d6",
    background = "#1a1b26",
    cursor_bg = "#c0caf5",
    cursor_border = "#c0caf5",
    cursor_fg = "#1a1b26",
    selection_bg = "#33467C",
    selection_fg = "#c0caf5",
    ansi = { "#15161E", "#f7768e", "#9ece6a", "#e0af68", "#7aa2f7", "#bb9af7", "#7dcfff", "#a9b1d6" },
    brights = { "#414868", "#f7768e", "#9ece6a", "#e0af68", "#7aa2f7", "#bb9af7", "#7dcfff", "#c0caf5" },
  },
  font_size = 18,
  warn_about_missing_glyphs = false,
  enable_tab_bar = false,
  window_close_confirmation = "NeverPrompt",
  window_background_opacity = 0.9,
  -- cursor_style = "Underline"
  -- ratelimit_output_bytes_per_second = 4000000,
}
