# Defined via `source`
function demacs --wraps='open -na "Emacs.app"' --description 'alias demacs open -na "Emacs.app"'
  open -na "Emacs.app" $argv; 
end
