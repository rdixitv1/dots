#!/usr/bin/env bash

wpdir="$HOME/wallpapers/pics/rd"
wp="$(ls "$wpdir" | sort -R | head -n 1)"
swaylock -i "$wpdir/$wp"
