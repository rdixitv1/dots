# Defined via `source`
function cdli --wraps='cd ; ls' --wraps='cd  && ls' --description 'alias cdli cd  && ls'
  cd  && ls $argv; 
end
