{-# LANGUAGE LambdaCase #-}

module XMonad.Custom.GridSelect where

import XMonad
import XMonad.Custom.Variables
import XMonad.Actions.GridSelect

myColorizer :: Window -> Bool -> X (String, String)
myColorizer = colorRangeFromClassName
                  (0x1a,0x1b,0x26) -- lowest inactive bg
                  (0x1a,0x1b,0x26) -- highest inactive bg
                  (0xc7,0x92,0xea) -- active bg
                  (0xc0,0xa7,0x9a) -- inactive fg
                  (0x1a,0x1b,0x29) -- active fg

-- gridSelect menu layout
myGridConfig :: p -> GSConfig Window
myGridConfig colorizer = (buildDefaultGSConfig myColorizer)
    { gs_cellheight   = 40
    , gs_cellwidth    = 200
    , gs_cellpadding  = 6
    , gs_originFractX = 0.5
    , gs_originFractY = 0.5
    , gs_font         = myFont
    }

spawnSelected' :: [(String, String)] -> X ()
spawnSelected' lst = gridselect conf lst >>= flip whenJust spawn
    where conf = def
                   { gs_cellheight   = 40
                   , gs_cellwidth    = 200
                   , gs_cellpadding  = 6
                   , gs_originFractX = 0.5
                   , gs_originFractY = 0.5
                   , gs_font         = myFont
                   }


gamesAppGrid =
    [ ("Crawl", myTerminal ++ " -e crawl") ]

eduAppGrid =
    [ ("Anki", "anki")
    , ("Qalculate", "qalculate-gtk")
    ]

internetAppGrid =
    [ ("Firefox", "firefox")
    , ("Firefox Dev", "firefox-developer-edition")
    , ("Brave", "brave")
    , ("Qutebrowser", "qutebrowser")
    , ("Microsoft Teams", "teams")
    ]

systemAppGrid =
    [ ("Wezterm", "wezterm") 
    , ("Alacritty", "alacritty")
    , ("Kitty", "kitty")
    , ("Bash", (myTerminal ++ " -e bash"))
    , ("Zsh", (myTerminal ++ " -e zsh"))
    , ("Fish", (myTerminal ++ " -e fish"))
    , ("Btop", (myTerminal ++ " -e btop"))
    , ("Htop", (myTerminal ++ " -e htop"))
    , ("Pcmanfm", "pcmanfm")
    , ("Pcmanfm Qt", "pcmanfm-qt")
    , ("VirtualBox", "virtualbox")
    , ("Virt-Manager", "virt-manager")
    ]

utilAppGrid =
  [ ("Emacs", "emacs")
  , ("Emacsclient", "emacsclient -c -a 'emacs'")
  , ("Nitrogen", "nitrogen")
  , ("Vim", (myTerminal ++ " -e vim"))
  , ("Neovim", (myTerminal ++ " -e nvim"))
  ]


officeAppGrid =
  [ ("Document Viewer", "zathura")
  , ("LibreOffice", "libreoffice")
  , ("LibreOffice Base", "lobase")
  , ("LibreOffice Calc", "localc")
  , ("LibreOffice Draw", "lodraw")
  , ("LibreOffice Impress", "loimpress")
  , ("LibreOffice Math", "lomath")
  , ("LibreOffice Writer", "lowriter")
  ]

settingsAppGrid =
    [ ("ARandR", "arandr")
    , ("ArchLinux Tweak Tool", "archlinux-tweak-tool")
    , ("Customize Look and Feel", "lxappearance")
    , ("Qt5 Settings", "qt5ct")
    , ("Kvantum Manager", "kvantummanager")
    ]

multimediaAppGrid =
  [ ("Audacity", "audacity")
  , ("Blender", "blender")
  , ("Deadbeef", "deadbeef")
  , ("Kdenlive", "kdenlive")
  , ("OBS Studio", "obs")
  , ("VLC", "vlc")
  ]

allAppGrids =
    [ ("Education", "/home/rd/.config/xmonad/scripts/xdotool-keychord.sh super+g 1")
    , ("Games", "/home/rd/.config/xmonad/scripts/xdotool-keychord.sh super+g 2")
    , ("Internet", "/home/rd/.config/xmonad/scripts/xdotool-keychord.sh super+g 3")
    , ("Multimedia", "/home/rd/.config/xmonad/scripts/xdotool-keychord.sh super+g 4")
    , ("Office", "/home/rd/.config/xmonad/scripts/xdotool-keychord.sh super+g 5")
    , ("Settings", "/home/rd/.config/xmonad/scripts/xdotool-keychord.sh super+g 6")
    , ("System", "/home/rd/.config/xmonad/scripts/xdotool-keychord.sh super+g 7")
    , ("Utilities", "/home/rd/.config/xmonad/scripts/xdotool-keychord.sh super+g 8")
    ]
