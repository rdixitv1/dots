;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets. It is optional.
;; (setq user-full-name "John Doe"
;;       user-mail-address "john@doe.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom:
;;
;; - `doom-font' -- the primary font to use
;; - `doom-variable-pitch-font' -- a non-monospace font (where applicable)
;; - `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;; - `doom-symbol-font' -- for symbols
;; - `doom-serif-font' -- for the `fixed-pitch-serif' face
;;
;; See 'C-h v doom-font' for documentation and more examples of what they
;; accept. For example:
;;
;;(setq doom-font (font-spec :family "Fira Code" :size 12 :weight 'semi-light)
;;      doom-variable-pitch-font (font-spec :family "Fira Sans" :size 13))
;;
;; If you or Emacs can't find your font, use 'M-x describe-font' to look them
;; up, `M-x eval-region' to execute elisp code, and 'M-x doom/reload-font' to
;; refresh your font settings. If Emacs still can't find your font, it likely
;; wasn't installed correctly. Font issues are rarely Doom issues!

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-one)

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")


;; Whenever you reconfigure a package, make sure to wrap your config in an
;; `after!' block, otherwise Doom's defaults may override your settings. E.g.
;;
;;   (after! PACKAGE
;;     (setq x y))
;;
;; The exceptions to this rule:
;;
;;   - Setting file/directory variables (like `org-directory')
;;   - Setting variables which explicitly tell you to set them before their
;;     package is loaded (see 'C-h v VARIABLE' to look up their documentation).
;;   - Setting doom variables (which start with 'doom-' or '+').
;;
;; Here are some additional functions/macros that will help you configure Doom.
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;; Alternatively, use `C-h o' to look up a symbol (functions, variables, faces,
;; etc).
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.


(setq eshell-rc-script "~/.config/doom/eshell/profile"
      eshell-visual-commands '("bash" "fish" "htop" "ssh" "top" "zsh"))

(setq shell-file-name (executable-find "fish"))

(setq doom-font (font-spec :family "JetBrainsMono NF" :size 24)
      doom-variable-pitch-font (font-spec :family "Fira Sans" :size 15)
      doom-big-font (font-spec :family "JetBrainsMono NF" :size 28))

(after! doom-themes
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t)
    (doom-themes-visual-bell-config))
(custom-set-faces!
  '(font-lock-comment-face :slant italic)
  '(font-lock-keyword-face :slant italic))
(setq global-prettify-symbols-mode t)

(global-visual-line-mode t)
(setq kill-whole-line t)

(defun rd/year-calendar (&optional year)
  (interactive)
  (require 'calendar)
  (let* (
      (current-year (number-to-string (nth 5 (decode-time (current-time)))))
      (month 0)
      (year (if year year (string-to-number (format-time-string "%y" (current-time))))))
    (switch-to-buffer (get-buffer-create calendar-buffer))
    (when (not (eq major-mode 'calendar-mode))
      (calendar-mode))
    (setq displayed-month month)
    (setq displayed-year year)
    (setq buffer-read-only nil)
    (erase-buffer)
    ;; horizontal rows
    (dotimes (j 4)
      ;; vertical columns
      (dotimes (i 3)
        (calendar-generate-month
          (setq month (+ month 1))
          year
          ;; indentation / spacing between months
          (+ 5 (* 25 i))))
      (goto-char (point-max))
      (insert (make-string (- 10 (count-lines (point-min) (point-max))) ?\n))
      (widen)
      (goto-char (point-max))
      (narrow-to-region (point-max) (point-max)))
    (widen)
    (goto-char (point-min))
    (setq buffer-read-only t)))

(defun rd/scroll-year-calendar-forward (&optional arg event)
  "scroll the yearly calendar by year in a forward direction."
  (interactive (list (prefix-numeric-value current-prefix-arg)
                     last-nonmenu-event))
  (unless arg (setq arg 0))
  (save-selected-window
    (if (setq event (event-start event)) (select-window (posn-window event)))
    (unless (zerop arg)
      (let* (
              (year (+ displayed-year arg)))
        (rd/year-calendar year)))
    (goto-char (point-min))
    (run-hooks 'calendar-move-hook)))

(defun rd/scroll-year-calendar-backward (&optional arg event)
  "scroll the yearly calendar by year in a backward direction."
  (interactive (list (prefix-numeric-value current-prefix-arg)
                     last-nonmenu-event))
  (rd/scroll-year-calendar-forward (- (or arg 1)) event))

(map! :leader
      :desc "scroll year calendar backward" "<left>" #'rd/scroll-year-calendar-backward
      :desc "scroll year calendar forward" "<right>" #'rd/scroll-year-calendar-forward)

(defalias 'year-calendar 'rd/year-calendar)
;; (use-package calfw)
;; (use-package calfw-org)

(map! :leader
      (:prefix ("c h" . "Help info from clippy")
      :desc "Describe function under pointer" "f" #'clippy-describe-function
      :desc "Describe variable under pointer" "v" #'clippy-describe-variable))


(map! :leader
      :desc "Edit doom config"
      "- d c" #'(lambda () (interactive) (find-file "~/.config/doom/config.el"))
      :leader
      :desc "Edit doom packages"
      "- d p" #'(lambda () (interactive) (find-file "~/.config/doom/packages.el"))
      :leader
      :desc "Edit doom init"
      "- d i" #'(lambda () (interactive) (find-file "~/.config/doom/init.el"))
      :leader
      :desc "Edit eshell aliases"
      "- d a" #'(lambda () (interactive) (find-file "~/.config/doom/eshell/aliases"))
      :leader
      :desc "Edit fish config"
      "- f" #'(lambda () (interactive) (find-file "~/.config/fish/config.fish"))
      :leader
      :desc "Edit vifm config"
      "- v c" #'(lambda () (interactive) (find-file "~/.config/vifm/vifmrc"))
      :leader
      :desc "Edit agenda"
      "- o" #'(lambda () (interactive) (find-file "~/org/agenda.org"))
      :leader
      :desc "Rust Mode"
      "- r" #'rust-mode
      :leader
      :desc "Autocomplete mode"
      "- a" #'auto-complete-mode
      :leader
      :desc "Company Mode"
      "- c" #'company-mode
      :leader
      :desc "Format file"
      "- b" #'eglot-format
      :leader
      :desc "Toggle Neotree"
      "o n" #'neotree-toggle
      :leader
      :desc "Open Emacs Keybindings Cheat Sheet"
      "- e" #'(lambda () (interactive) (find-file "~/org-mode/emacs-vs-evil.org"))
      :leader
      :desc "Open mu4e"
      "o m" #'mu4e
      :leader
      :desc "Open ERC with Liberachat"
      "- l" #'(lambda ()
                (interactive)
                (erc-tls :server "irc.libera.chat"
                         :port "6667")))
(after! org
  (defun rd/org-font-setup ()
	  ;; Replace list hyphen with dot
	  (font-lock-add-keywords 'org-mode
				  '(("^ *\\([-]\\) "
				     (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))

	  ;; Set faces for heading levels
	  (dolist (face '((org-level-1 . 1.7)
			  (org-level-2 . 1.5)
			  (org-level-3 . 1.4)
			  (org-level-4 . 1.3)
			  (org-level-5 . 1.2)
			  (org-level-6 . 1.1)
			  (org-level-7 . 1.1)
			  (org-level-8 . 1.1)))
	    (set-face-attribute (car face) nil :font "Fira Sans" :weight 'regular :height (cdr face)))

	  ;; Ensure that anything that should be fixed-pitch in Org files appears that way
	  (set-face-attribute 'org-block nil :foreground nil :inherit 'fixed-pitch)
	  (set-face-attribute 'org-code nil   :inherit '(shadow fixed-pitch))
	  (set-face-attribute 'org-table nil   :inherit '(shadow fixed-pitch))
	  (set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))
	  (set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
	  (set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch))
	  (set-face-attribute 'org-checkbox nil :inherit 'varible-pitch))

  (defun rd/org-mode-start ()
    (interactive)
    (org-bullets-mode 1)
    (display-line-numbers-mode 0)
    (visual-fill-column-mode 1))

   (setq org-roam-directory "~/org/roam/")
   (setq org-roam-index-file "~/org/roam/index.org")
   (setq org-agenda-files '("~/org/agenda.org"))
   (setq visual-fill-column-width 100
         visual-fill-column-center-text t)
  (visual-fill-column-mode 1)
  (rd/org-mode-start))

(use-package org
  :config
  (setq org-directory "~/org/"
        org-default-notes-file (expand-file-name "notes.org" org-directory)
        org-ellipsis " ▼ "
        org-log-done 'time
        org-hide-emphasis-markers t
        org-todo-keywords
        '((sequence
           "TODO(t)"
           "HOMEWORK(h)"
           "CLASS(C)"
           "PROJ(p)"
           "TEST(T)"
           "EVENT(e)"
           "EXAM(E)"
           "|"
           "DONE(d)"
           "CANCELLED(c)"
           "ANYTIME(a)")))
  (rd/org-font-setup))


(setq
   org-fancy-priorities-list '("🟥" "🟦" "🟩")
   org-priority-faces
   '((?A :foreground "#f7768e" :weight bold)
     (?B :foreground "#9ece6a" :weight bold)
     (?C :foreground "#bb9af7" :weight bold))
   org-agenda-block-separator 8411)
(setq org-agenda-custom-commands
      '(("v" "A better agenda view"
         ((tags "PRIORITY=\"A\""
                ((org-agenda-skip-function '(org-agenda-skip-entry-if 'todo 'done))
                 (org-agenda-overriding-header "High priority:")))
          (tags "PRIORITY=\"B\""
                ((org-agenda-skip-function '(org-agenda-skip-entry-if 'todo 'done))
                 (org-agenda-overriding-header "Medium priority:")))
          (tags "PRIORITY=\"C\""
                ((org-agenda-skip-function '(org-agenda-skip-entry-if 'todo 'done))
                 (org-agenda-overriding-header "Low priority:")))
          (tags "school"
                ((org-agenda-skip-function '(org-agenda-skip-entry-if 'todo 'done))
                 (org-agenda-overriding-header "School:")))
          (tags "classes"
                ((org-agenda-skip-function '(org-agenda-skip-entry-if 'todo 'done))
                 (org-agenda-overriding-header "Classes:")))
          (tags "personal"
                ((org-agenda-skip-function '(org-agenda-skip-entry-if 'todo 'done))
                 (org-agenda-overriding-header "Personal")))
          (agenda "")
          (alltodo "")))))


(setq confirm-kill-emacs nil)
(setq auto-save-default t
      make-backup-files t)

(evil-define-key 'normal dired-mode-map
  (kbd "M-RET") 'dired-display-file
  (kbd "h") 'dired-up-directory
  (kbd "l") 'dired-open-file ; use dired-find-file instead of dired-open.
  (kbd "m") 'dired-mark
  (kbd "t") 'dired-toggle-marks
  (kbd "u") 'dired-unmark
  (kbd "C") 'dired-do-copy
  (kbd "D") 'dired-do-delete
  (kbd "J") 'dired-goto-file
  (kbd "M") 'dired-chmod
  (kbd "O") 'dired-chown
  (kbd "P") 'dired-do-print
  (kbd "R") 'dired-rename
  (kbd "T") 'dired-do-touch
  (kbd "Y") 'dired-copy-filenamecopy-filename-as-kill ; copies filename to kill ring.
  (kbd "+") 'dired-create-directory
  (kbd "-") 'dired-up-directory
  (kbd "% l") 'dired-downcase
  (kbd "% u") 'dired-upcase
  (kbd "; d") 'epa-dired-do-decrypt
  (kbd "; e") 'epa-dired-do-encrypt)
;; If peep-dired is enabled, you will get image previews as you go up/down with 'j' and 'k'
(evil-define-key 'normal peep-dired-mode-map
  (kbd "j") 'peep-dired-next-file
  (kbd "k") 'peep-dired-prev-file)
(add-hook 'peep-dired-hook 'evil-normalize-keymaps)
;; Get file icons in dired
(add-hook 'dired-mode-hook 'all-the-icons-dired-mode)
;; With dired-open plugin, you can launch external programs for certain extensions
;; For example, I set all .png files to open in 'sxiv' and all .mp4 files to open in 'mpv'
(use-package peep-dired
  :defer t ; don't access `dired-mode-map' until `peep-dired' is loaded
  :bind (:map dired-mode-map
         ("P" . peep-dired)))
  (use-package dired-single
    :defer t)

  (use-package dired-ranger
    :defer t)

  (use-package dired-collapse
    :defer t)
(use-package evil-collection)
  (evil-collection-define-key 'normal 'dired-mode-map
    "H" 'dired-omit-mode
    "y" 'dired-ranger-copy
    "X" 'dired-ranger-move
    "p" 'dired-ranger-paste)

(require 'evil-collection)

(map! :leader
      :desc "EWW web browser"
      "e w" #'eww
      :leader
      :desc "EWW reload page"
      "e R" #'eww-reload
      :leader
      :desc "Search web for text between BEG/END"
      "s w" #'eww-search-words)

(use-package dashboard
  :init
  (setq dashboard-set-heading-icons t)
  (setq dashboard-set-file-icons t)
  (setq dashboard-banner-logo-title "Welcome to Emacs!")
  (setq dashboard-startup-banner 'logo)) ; Makes logo Default banner
  ;; (setq dashboard-startup-banner "~/.config/doom/logo.jpg")  ; use custom image as banner
  (setq dashboard-center-content t)
  (setq dashboard-items '((recents . 5)
                          (agenda . 5 )
                          (bookmarks . 5)
                          (projects . 5)
                          (registers . 5)))
(defun prefer-horizontal-split ()
  (set-variable 'split-height-threshold nil t)
  (set-variable 'split-width-threshold 40 t)) ; make this as low as needed
(add-hook 'markdown-mode-hook 'prefer-horizontal-split)
(map! :leader
      :desc "Clone indirect buffer other window" "b c" #'clone-indirect-buffer-other-window)

;; (use-package fira-code-mode
;   :config (global-fira-code-mode)
  ;; :custom (fira-code-mode-disabled-ligatures '("[]" ":" "x")))
;; (defconst jetbrains-ligature-mode--ligatures
;;    '("-->" "//" "/**" "/*" "*/" "<!--" ":=" "->>" "<<-" "->" "<-"
;;      "<=>" "==" "!=" "<=" ">=" "=:=" "!==" "&&" "||" "..." ".."
;;      "|||" "///" "&&&" "===" "++" "--" "=>" "|>" "<|" "||>" "<||"
;;      "|||>" "<|||" ">>" "<<" "::=" "|]" "[|" "{|" "|}"
;;      "[<" ">]" ":?>" ":?" "/=" "[||]" "!!" "?:" "?." "::"
;;      "+++" "??" "###" "##" ":::" "####" ".?" "?=" "=!=" "<|>"
;;      "<:" ":<" ":>" ">:" "<>" "***" ";;" "/==" ".=" ".-" "__"
;;      "=/=" "<-<" "<<<" ">>>" "<=<" "<<=" "<==" "<==>" "==>" "=>>"
;;      ">=>" ">>=" ">>-" ">-" "<~>" "-<" "-<<" "=<<" "---" "<-|"
;;      "<=|" "/\\" "\\/" "|=>" "|~>" "<~~" "<~" "~~" "~~>" "~>"
;;      "<$>" "<$" "$>" "<+>" "<+" "+>" "<*>" "<*" "*>" "</>" "</" "/>"
;;      "<->" "..<" "~=" "~-" "-~" "~@" "^=" "-|" "_|_" "|-" "||-"
;;      "|=" "||=" "#{" "#[" "]#" "#(" "#?" "#_" "#_(" "#:" "#!" "#="
;;      "&="))

;; (sort jetbrains-ligature-mode--ligatures (lambda (x y) (> (length x) (length y))))

;; (dolist (pat jetbrains-ligature-mode--ligatures)
;;   (set-char-table-range composition-function-table
;;                       (aref pat 0)
;;                       (nconc (char-table-range composition-function-table (aref pat 0))
;;                              (list (vector (regexp-quote pat)
;;                                            0
;;                                     'compose-gstring-for-graphic)))))

(after! lsp-ui
  (setq lsp-ui-doc-enable t))

; PDFS
(use-package pdf-tools
  :defer t
  :commands (pdf-loader-install)
  :mode "\\.pdf\\'"
  :bind (:map pdf-view-mode-map
              ("j" . pdf-view-next-line-or-next-page)
              ("k" . pdf-view-previous-line-or-previous-page)
              ("=" . pdf-view-enlarge)
              ("-" . pdf-view-shrink))
  :init (pdf-loader-install)
  :config (add-to-list 'revert-without-query ".pdf"))

(add-hook 'pdf-view-mode-hook #'(lambda () (interactive) (display-line-numbers-mode -1)))

; EDIFF
(setq ediff-split-window-function 'split-window-horizontally
      ediff-window-setup-function 'ediff-setup-windows-plain)

(defun rd-ediff-hook ()
  (ediff-setup-keymap)
  (define-key ediff-mode-map "j" 'ediff-next-difference)
  (define-key ediff-mode-map "k" 'ediff-previous-difference))

(setq doom-modeline-height 30     ;; sets modeline height
      doom-modeline-bar-width 5   ;; sets right bar width
      doom-modeline-persp-name t  ;; adds perspective name to modeline
      doom-modeline-persp-icon t) ;; adds folder icon next to persp name
