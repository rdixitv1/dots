#!/usr/bin/env bash

wpdir="$HOME/wallpapers/pics/rd"
wp="$(ls "$wpdir" | sort -R | head -n 1)"
i3lock -i "$wpdir/$wp"
