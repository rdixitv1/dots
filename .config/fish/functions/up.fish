function up --argument arg
if [ $arg = "pac" ]
doas pacman -Syu
else if [ $arg = "aur" ]
paru -Syu
else if [ $arg = "arch" ]
doas pacman -Syu
paru -Syu
else if [ $arg = "rust" ]
cargo install-update -a
else
doas pacman -Syu
paru -Syu
cargo install-update -a
end
end
