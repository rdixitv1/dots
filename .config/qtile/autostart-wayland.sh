#!/usr/bin/env bash

swaybg -m fill -i "$HOME/wallpapers/pics/TokyoNight/tux.png" &
lxqt-policykit-agent &
emacs --daemon &
volumeicon &
ibus-daemon &
# nm-applet &
nm-tray &
