#!/usr/bin/env bash

lxqt-policykit-agent &
picom &
/usr/bin/emacs --daemon &
# nm-applet &
nm-tray &
feh -zr --bg-fill --no-fehbg /home/rd/wallpapers/pics/rd &
dunst &
volumeicon &
ibus-daemon &
xrandr -s 1920x1200
#sxhkd &
