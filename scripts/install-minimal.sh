#!/usr/bin/env bash

echo "This script will deploy my dotfiles and install needed packages."
sleep 3

sudo pacman -Syu --noconfirm
sudo pacman -S --needed stow git awesome wezterm rofi emacs fish neovim starship

# Deploy
stow .

# Doom Emacs
git clone --depth 1 https://github.com/doomemacs/doomemacs ~/.emacs.d
~/.emacs.d/bin/doom --force install
~/.emacs.d/bin/doom sync
