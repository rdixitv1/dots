# make sxhkd reload its configuration files:
super + Escape
	pkill -USR1 -x sxhkd


# BSPWM HOTKEYS

# quit/restart bspwm
super + shift + {q,r}
	bspc {quit,wm -r}

# close and kill
super + shift + c
	bspc node -c

# alternate between the tiled and monocle layout
super + m
	bspc desktop -l next

# send the newest marked node to the newest preselected node
super + y
	bspc node newest.marked.local -n newest.!automatic.local

# swap the current node and the biggest node
super + g
	bspc node -s biggest


# STATE/FLAGS

# set the window state
super + {t,shift + t,s,f}
	bspc node -t {tiled,pseudo_tiled,floating,fullscreen}

# set the node flags
super + ctrl + {m,x,y,z}
	bspc node -g {marked,locked,sticky,private}


# FOCUS/SWAP

# focus the node in the given direction
super + {_,shift + }{h,j,k,l}
	bspc node -{f,s} {west,south,north,east}

# focus the node for the given path jump
super + {shift+p,shift+b,comma,period}
	bspc node -f @{parent,brother,first,second}

# focus the next/previous node in the current desktop
super + {_,shift + }n
	bspc node -f {next,prev}.local

# focus the next/previous desktop in the current monitor
super + bracket{left,right}
	bspc desktop -f {prev,next}.local

# focus the last node/desktop
super + {grave,Tab}
	bspc {node,desktop} -f last

# focus the older or newer node in the focus history
super + {o,i}
	bspc wm -h off; \
	bspc node {older,newer} -f; \
	bspc wm -h on

# focus or send to the given desktop
super + {_,shift + }{1-9,0}
	bspc {desktop -f,node -d} focused:'^{1-9,10}'


# PRESELECT

# preselect the direction
super + ctrl + {h,j,k,l}
	bspc node -p {west,south,north,east}

# preselect the ratio
super + ctrl + {1-9}
	bspc node -o 0.{1-9}

# cancel the preselection for the focused node
super + ctrl + space
	bspc node -p cancel

# cancel the preselection for the focused desktop
super + ctrl + shift + space
	bspc query -N -d | xargs -I id -n 1 bspc node id -p cancel


# MOVE/RESIZE

# expand a window by moving one of its side outward
super + alt + {h,j,k,l}
	bspc node -z {left -20 0,bottom 0 20,top 0 -20,right 20 0}

# contract a window by moving one of its side inward
super + alt + shift + {h,j,k,l}
	bspc node -z {right -20 0,top 0 20,bottom 0 -20,left 20 0}

# move a floating window
super + {Left,Down,Up,Right}
	bspc node -v {-20 0,0 20,0 -20,20 0}


# APPLICATION KEYBINDINGS (Super + Alt + Key)

# terminal emulator
super + Return
	alacritty
    
super + b ; {super + b,b,f,shift + f,z,shift + z,n,t,w,m,super + f,e,super + e,shift + e, c, p}
    {/opt/firefox-dev/firefox, brave-browser, pcmanfm, pcmanfm-qt, /home/rd/Applications/Zulip.AppImage, alacritty -e zulip-term,/opt/firefox-dev/firefox --private-window,brave --tor,kitty -e nmtui,alacritty -e btop,alacritty -e lf,qutebrowser teams.microsoft.com,teams,brave teams.microsoft.com, qalculate-gtk, pavucontrol}

super + b ; x ; {1,2}
    xrandr -s {1920x1202560x1600}

super + f ; f ; {f,s}
  {flameshot full -p ~/ScreenShots,flameshot gui}

super + f ; c ; f
  flameshot full -c

super + f ; g
  flameshot launcher

super + f ; x
  xfce4-screenshooter

alt + shift + Return ; {f,z,b}
  alacritty -e {fish,zsh,bash}

alt + shift + Return ; k
  kitty

super + x ; {super + x,l}
  {archlinux-logout,/home/rd/.local/bin/lock}

super + e ; super + e
      emacsclient -c -a 'emacs' -e '(dashboard-refresh-buffer)'

super + e ; {b,d,i,m,s,v}
    emacsclient -c -a 'emacs' -e {'(ibuffer)','(dired nil)','(erc)','(mu4e)','(eshell)','(+vterm/here nil)'}

super + p ; {b,e,c,h,shift + h,space,k,l,o,d,n,r,t,g,w,s,shift + w,p,Escape,Return}
    /home/rd/run-scripts/dm-{bookmarks,confedit,currencies,help,history,hub,kill,lg,maim,man,note,rec,translate,wall,weather,websearch,wifi,pass,power,menu}

super + shift + Return
    rofi -show drun

super + alt + Return
    rofi -show run -theme ~/.config/rofi/themes/top-tn.rasi

super + p ; a
  rofi -show calc -modi calc -no-show-match -no-sort

super + p ; f
    rofi -show filebrowser

super + shift + space ; {1,2,3,4,5,6,7,8}
	setxkbmap {intl,mtg,wmn,dk,fi,in -variant kan-kagapa,in -variant san-kagapa, ru -variant phonetic}

super + shift + u ; {super + shift + u,a,shift + a}
    xbacklight ={0,100,1}

super + shift + u ; {j,k}
    xbacklight -{dec,inc} 1
super + shift + u ; {a,shift + a, super + shift + u}
    xbacklight ={100,1,0}

super + shift + u ; {space, n, b}
    mpc {toggle, next, prev}

